<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * Dashboard. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.voopress.com
 * @since             1.0.0
 * @package           Polylang_Scanner
 *
 * @wordpress-plugin
 * Plugin Name:       Polylang Scanner
 * Plugin URI:        http://www.voopress.com/plugins/polylang-scanner/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress dashboard.
 * Version:           1.0.0
 * Author:            Vito Calderaro
 * Author URI:        http://vito.voopress.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       polylang-scanner
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once plugin_dir_path( __FILE__ ) . 'includes/class-polylang-scanner.php';

new Polylang_Scanner();

/* override _e() and __() with pll_e() and pll__() */
function polylang_scanner_filter_gettext( $translated, $original, $domain ) {
	if( $domain != 'pll_string' ) {
		$translated_pll = pll__($original, $domain);
		if($translated_pll != $original) {
			return $translated_pll;
		}
	}

	return $translated;
}
 
add_filter( 'gettext', 'polylang_scanner_filter_gettext', 20, 3 );
?>