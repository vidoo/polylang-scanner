<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the dashboard.
 *
 * @link       http://www.voopress.com
 * @since      1.0.0
 *
 * @package    Polylang_Scanner
 * @subpackage Polylang_Scanner/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, dashboard-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Polylang_Scanner
 * @subpackage Polylang_Scanner/includes
 * @author     Vito Calderaro <email@voopress.com>
 */

class Polylang_Scanner {
	public function __construct() {
		//delete_option('polylang_scanner_strings');
		//delete_option( 'polylang_scanner_dirs' );
		// adds the link to the languages panel in the wordpress admin menu
		add_action('admin_menu', array(&$this, 'add_menus'));
		add_action('wp_ajax_polylang_do_scan', array(&$this, 'do_scan'));
		add_action('plugins_loaded', array(&$this, 'load_registered_strings'));
		// embed the javascript file that makes the AJAX request
		//wp_enqueue_script( 'my-ajax-request', plugin_dir_url( __FILE__ ) . 'js/ajax.js', array( 'jquery' ) );
		// declare the URL to the file that handles the AJAX request (wp-admin/admin-ajax.php)
		//wp_localize_script( 'my-ajax-request', 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
	}

	public function add_menus() {
		add_submenu_page('tools.php', $title = __('Polylang scanner', 'polylang-scanner'), $title, 'manage_options', 'polylang-scanner', array(&$this, 'tools_page'));
	}

	public function find_activated_theme_plugins() {
		$dirs_to_scan = array();

		/* wordpress 3.4+ */
		$theme = wp_get_theme( );

		$dirs_to_scan[] = array(
			'dir' => $theme->stylesheet_dir,
			'name' => $theme->name,
			'type' => 'theme',
		);

		if( $theme->parent() ) {
			$dirs_to_scan[] = array(
				'dir' => $theme->template_dir,
				'name' => $theme->parent_theme,
				'type' => 'theme',
			);
		}

		$activated_plugins = get_option('active_plugins');

		foreach($activated_plugins as $value) {
			$path = WP_PLUGIN_DIR.DIRECTORY_SEPARATOR.$value;
			$plugin_data = get_plugin_data($path);

			$dirs_to_scan[] = array(
				'dir' => WP_PLUGIN_DIR.DIRECTORY_SEPARATOR.reset((explode('/',$value))),
				'name' => $plugin_data['Name'],
				'type' => 'plugin',
			);
		}

		return $dirs_to_scan;
	}

	public function glob_recursive($pattern, $flags = 0) {
		$files = glob($pattern, $flags);
		
		foreach (glob(dirname($pattern).DIRECTORY_SEPARATOR.'*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir)
		{
			$files = array_merge($files, $this->glob_recursive($dir.DIRECTORY_SEPARATOR.basename($pattern), $flags));
		}

		return $files;
	}

	public function do_scan() {
		$dirs_to_scan = array();
		$n_found = 0;

		if( !get_option( 'polylang_scanner_dirs' ) ) {
			$params = array();
			parse_str($_POST['dirs'], $params);
			$dirs_to_scan = $this->find_activated_theme_plugins();

			foreach($dirs_to_scan as $key => $dir){
				if( !in_array(md5($dir['dir']), $params['dirs']) ) {
					unset( $dirs_to_scan[$key] );
				}
			}

			$dirs_to_scan = array_values($dirs_to_scan);

			update_option( 'polylang_scanner_dirs', $dirs_to_scan );
		} else {
			$dirs_to_scan = get_option( 'polylang_scanner_dirs' );
		}

		$current_dir_to_scan = reset($dirs_to_scan);

		if( !empty($current_dir_to_scan) ) {
			$strings = array();
			$files = $this->glob_recursive($current_dir_to_scan['dir'].DIRECTORY_SEPARATOR."*.php");
			$md5 = $current_dir_to_scan['dir'];

			foreach ( $files as $filename ) {
				$file_content = file_get_contents($filename);

				if( preg_match_all('/[^\S]_[e_][^\S]*\([^)]*\)/', $file_content, $matches) ) {
					foreach($matches[0] as $match) {
						// TODO: find better regexes !
						if( preg_match_all('/_[e_][^\S]*\([^\S]*[\'"]([^)]*)[\'"][^\S]*,[^\S]*[\'"]([^)]*)[\'"][^\S]*\)/', $match, $data) )
						{
							$strings[] = array(
								'string' => $data[1][0],
								'domain' => $data[2][0],
								'md5' => $md5
							);
							$n_found++;
						} else if( preg_match_all('/_[e_][^\S]*\([^\S]*[\'"]([^)]*)[\'"][^\S]*\)/', $match, $data) ) {
							$strings[] = array(
								'string' => $data[1][0],
								'domain' => null,
								'md5' => $md5
							);
							$n_found++;
						}			
					}
				}
			}

			if( !empty($strings) ) {
				$previous_strings = get_option('polylang_scanner_strings');
				
				if(!empty($previous_strings)) {
					//remove old strings registered
					foreach ($previous_strings as $key => $value) {
						if($value['md5'] == $md5){
							unset($previous_strings[$key]);
						}
					}
					$strings = array_merge($previous_strings, $strings);
					$strings = array_map("unserialize", array_unique(array_map("serialize", $strings)));
				}
				update_option( 'polylang_scanner_strings', $strings );
			}

			reset( $dirs_to_scan );
			$index_scanned = key($dirs_to_scan);
			unset( $dirs_to_scan[$index_scanned] );
			update_option( 'polylang_scanner_dirs', $dirs_to_scan );
		}

		end($dirs_to_scan);
		$end_index = key($dirs_to_scan);

		if( empty($dirs_to_scan) ) {
			delete_option( 'polylang_scanner_dirs' );
			$done_index = $end_index;
			$end_index = $end_index;
		}else{
			$done_index = $index_scanned;
			$end_index = $end_index;
		}

		$response = json_encode( array(
			'success' => true,
			'dir' => $current_dir_to_scan['name'],
			'found' => $n_found,
			'done_index' => $done_index,
			'end_index' => $end_index
		) );

		header( "Content-Type: application/json" );
		echo $response;
		exit;
	}

	public function load_registered_strings() {
		$strings = get_option( 'polylang_scanner_strings');

		if( !empty($strings) ) {
			$strings = $strings;

			if( !empty($strings) ) {
				foreach ($strings as $val) {
					if(!empty($val['domain'])){
						pll_register_string($val['string'], $val['string'], $val['domain']);
					}else{
						pll_register_string($val['string'], $val['string']);
					}					
				}
			}
		}
	}

	public function tools_page() { ?>
		<div class="wrap">
			<?php screen_icon('tools'); ?>
			<?php
			global $_wp_admin_css_colors;
			$admin_colors = $_wp_admin_css_colors;
			if( !empty($admin_colors) ) {
				$color_over = $admin_colors[get_user_option('admin_color')]->colors[2];
			} else {
				$color_over = '#0074a2';
			}
			?>
			<h2>Polylang Scanner</h2>

			<div class="form-wrap">
				<form id="scan" method="post" action="">
					<input type="hidden" name="polylang-scanner" value="import" />
					<div id="polylang-scanner-progress" style="width:100%;height:25px;background:#d6d6d6;margin: 2em 0 1em;border-radius:3px;">
						<div class="progress" style="width:0%;height:25px;background:<?php echo $color_over; ?>;border-radius:3px;">
							
						</div>
					</div>
					<div class="current-log" style="margin: 0 0 2em 0">&nbsp;</div>
					<?php
					$attr = empty($deactivated) ? array() : array('disabled' => 'disabled');
					submit_button(__('Search strings'), 'primary', 'submit', true, $attr);
					?>
					
					<?php $dirs_to_scan = $this->find_activated_theme_plugins(); ?>
					<h3>Themes activated</h3>
					<?php
					foreach($dirs_to_scan as $dir){
						if( $dir['type'] == 'theme' ){
							$id = md5($dir['dir']);
							echo '<p><label style="display:inline-block" for="'.$id.'"><input id="'.$id.'" name="dirs[]" type="checkbox" value="'.$id.'">'.$dir['name'].'</label></p>';
						}					
					}
					?>

					<h3>Plugins activated</h3>
					<?php
					foreach($dirs_to_scan as $dir){
						if( $dir['type'] == 'plugin' ){
							$id = md5($dir['dir']);
							echo '<p><label style="display:inline-block" for="'.$id.'"><input id="'.$id.'" name="dirs[]" type="checkbox" value="'.$id.'">'.$dir['name'].'</label></p>';
						}					
					}
					?>

					<div class="full-log" style="display: none">
						<h3>Output :</h3>
						<div class="process" style="box-sizing:border-box;background:#ccc;width:100%; padding:1em; height:12em; overflow:auto;">
							
						</div>
					</div>
				</form>
			</div><!-- form-wrap -->
			<script>
			jQuery(document).ready( function($){
				function polylang_scan_progress() {
					$.ajax({
						type: 'POST',
						url: '<?php echo admin_url( 'admin-ajax.php' ) ?>',
						data: { action : 'polylang_do_scan', dirs : $('form#scan [name="dirs[]"]').serialize() },
						success: function( response ) {
							if( response.success == true ) {
								var progressbar = $('#polylang-scanner-progress .progress');
								var width = progressbar.width();
								var parentWidth = progressbar.offsetParent().width();
								var percent = 100*width/parentWidth;
								$('#polylang-scanner-progress .progress').css('width', ((response.done_index+1)*(100/(response.end_index+1)))+'%');
								if(response.found > 0) response.found = '<strong>'+response.found+'</strong>';
								if( response.found == 0) {
									log = response.dir + ' scanned : no string found<br>';
								}else{
									log = response.dir + ' scanned : '+ response.found +' strings registered<br>';
								}
								
								$('#scan .current-log').html( log )
								$('#scan .process').append( log );

								if( response.done_index < response.end_index ) {
									polylang_scan_progress();
								}
							}else{
								$('#scan .process').append('<strong>ERROR:</strong> '+response+'<br>');
							}
						},
						dataType: 'json',
						async: true
					});
				}

				$('form#scan').submit( function(e) {
					e.preventDefault();
					$('#scan .process').html('');
					$('#polylang-scanner-progress .progress').css('width', 0);
					polylang_scan_progress();
				});
			});
			
			</script>
		</div><!-- wrap --><?php
	}
}